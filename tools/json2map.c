/*
RvnicRaven retro game engine

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define CRUSH_C
#include "../external/crush.c"

#define HLH_JSON_IMPLEMENTATION
#include "../external/HLH_json.h"
//-------------------------------------

//Internal includes
//-------------------------------------

//#defines
#define READ_ARG(I) \
   ((++(I))<argc?argv[(I)]:NULL)
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
static uint16_t *level_texture = NULL;
static int8_t *level_floor = NULL;
static int8_t *level_ceiling = NULL;
static uint16_t level_width = 0;
static uint16_t level_height = 0;
static uint8_t level_floor_color = 23;
static uint16_t player_x;
static uint16_t player_y;
static uint16_t player_z;
static uint16_t player_angle;
//-------------------------------------

//Function prototypes
static void map_write(const char *path);
static void util_mem_compress(void *mem, int32_t length, FILE *out);
static void print_help(char **argv);
//-------------------------------------

//Function implementations

int main(int argc, char **argv)
{
   const char *path_project = NULL;
   const char *path_level = NULL;
   const char *path_out = NULL;

   for(int i = 1;i<argc;i++)
   {
      if(strcmp(argv[i],"--help")==0||
         strcmp(argv[i],"-help")==0||
         strcmp(argv[i],"-h")==0||
         strcmp(argv[i],"?")==0)
      { print_help(argv); return 0; }
      else if(strcmp(argv[i],"-project")==0)
         path_project = READ_ARG(i);
      else if(strcmp(argv[i],"-level")==0)
         path_level = READ_ARG(i);
      else if(strcmp(argv[i],"-fout")==0)
         path_out = READ_ARG(i);
   }

   if(path_project==NULL)
   {
      printf("project file not specified, try %s --help for more info\n",argv[0]);
      return 0;
   }
   if(path_level==NULL)
   {
      printf("level file not specified, try %s --help for more info\n",argv[0]);
      return 0;
   }

   HLH_json5_root *json_data = HLH_json_parse_file(path_project);
   HLH_json5_root *json = HLH_json_parse_file(path_level);
   HLH_json5 *tiles = NULL;
   HLH_json5 *ceilings = NULL;
   HLH_json5 *floors = NULL;
   HLH_json5 *entities = NULL;
   int grid_size = 0;
   int tileset_width = 0;
   int tileset_height = 0;
   int tile_dim = 0;

   //Parse basic information
   HLH_json5 *layer_instances = HLH_json_get_object(&json->root,"layerInstances");
   for(int i = 0;i<HLH_json_get_array_size(layer_instances);i++)
   {
      HLH_json5 *layer_instance = HLH_json_get_array_item(layer_instances,i);
      const char *identifier = HLH_json_get_object_string(layer_instance,"__identifier","(NULL)");
      if(strcmp(identifier,"Tiles")==0||strcmp(identifier,"Ceiling")==0||strcmp(identifier,"Floor")==0||strcmp(identifier,"Entities")==0)
      {
         level_width = HLH_json_get_object_integer(layer_instance,"__cWid",16);
         level_height = HLH_json_get_object_integer(layer_instance,"__cHei",16);
         grid_size = HLH_json_get_object_integer(layer_instance,"__gridSize",16);
      }

      if(strcmp(identifier,"Tiles")==0)
         tiles = HLH_json_get_object(layer_instance,"gridTiles");
      if(strcmp(identifier,"Ceiling")==0)
         ceilings = HLH_json_get_object(layer_instance,"intGrid");
      if(strcmp(identifier,"Floor")==0)
         floors = HLH_json_get_object(layer_instance,"intGrid");
      if(strcmp(identifier,"Entities")==0)
         entities = HLH_json_get_object(layer_instance,"entityInstances");
   }
   HLH_json5 *tileset = HLH_json_get_array_item(HLH_json_get_object(HLH_json_get_object(&json_data->root,"defs"),"tilesets"),0);
   tileset_width = HLH_json_get_object_integer(tileset,"__cWid",16);
   tileset_height = HLH_json_get_object_integer(tileset,"__cHei",16);
   tile_dim = HLH_json_get_object_integer(tileset,"tileGridSize",16);
   //-------------------------------------

   //Parse field instances (custom variables
   HLH_json5 *field_instances = HLH_json_get_object(&json->root,"fieldInstances");
   for(int i = 0;i<HLH_json_get_array_size(field_instances);i++)
   {
      HLH_json5 *field_instance = HLH_json_get_array_item(field_instances,i);
      const char *identifier = HLH_json_get_object_string(field_instance,"__identifier","(NULL)");
      if(strcmp(identifier,"floor_color")==0)
         level_floor_color = HLH_json_get_object_integer(field_instance,"__value",0);
   }
   //-------------------------------------

   //Allocate memory for map data
   level_texture = calloc(level_width*level_height,sizeof(*level_texture));
   level_floor = calloc(level_width*level_height,sizeof(*level_floor));
   level_ceiling = calloc(level_width*level_height,sizeof(*level_ceiling));
   //-------------------------------------

   //Read textures
   for(int i = 0;i<HLH_json_get_array_size(tiles);i++)
   {
      HLH_json5 *tile = HLH_json_get_array_item(tiles,i);
      int index = (HLH_json_get_array_integer(HLH_json_get_object(tile,"px"),1,0)/grid_size)*level_width+(HLH_json_get_array_integer(HLH_json_get_object(tile,"px"),0,0)/grid_size);
      int tex_index = (HLH_json_get_array_integer(HLH_json_get_object(tile,"src"),1,0)/tile_dim)*tileset_width+(HLH_json_get_array_integer(HLH_json_get_object(tile,"src"),0,0)/tile_dim);
      level_texture[index] = tex_index;
   }
   //-------------------------------------

   //Read ceiling height
   for(int i = 0;i<HLH_json_get_array_size(ceilings);i++)
   {
      HLH_json5 *ceiling = HLH_json_get_array_item(ceilings,i);
      int index = HLH_json_get_object_integer(ceiling,"coordId",0);
      level_ceiling[index] = HLH_json_get_object_integer(ceiling,"v",0)-128;
   }
   //-------------------------------------

   //Read floor height
   for(int i = 0;i<HLH_json_get_array_size(floors);i++)
   {
      HLH_json5 *floor = HLH_json_get_array_item(floors,i);
      int index = HLH_json_get_object_integer(floor,"coordId",0);
      level_floor[index] = HLH_json_get_object_integer(floor,"v",0)-128;
   }
   //-------------------------------------

   //Read entities
   for(int i = 0;i<HLH_json_get_array_size(entities);i++)
   {
      HLH_json5 *entity = HLH_json_get_array_item(entities,i);
      if(strcmp(HLH_json_get_object_string(entity,"__identifier","(NULL"),"Player")==0)
      {
         player_x = HLH_json_get_array_integer(HLH_json_get_object(entity,"__grid"),0,0)*1024;
         player_y = HLH_json_get_array_integer(HLH_json_get_object(entity,"__grid"),1,0)*1024;
         HLH_json5 *field_instances = HLH_json_get_object(entity,"fieldInstances");
         for(int j = 0;j<HLH_json_get_array_size(field_instances);j++)
         {
            HLH_json5 *field_instance = HLH_json_get_array_item(field_instances,j);
            if(strcmp(HLH_json_get_object_string(field_instance,"__identifier","(NULL)"),"z")==0)
               player_z = HLH_json_get_object_integer(field_instance,"__value",0);
            else if(strcmp(HLH_json_get_object_string(field_instance,"__identifier","(NULL)"),"angle")==0)
               player_angle = HLH_json_get_object_integer(field_instance,"__value",0);
         }
      }
   }
   //-------------------------------------

   //Write map file
   if(path_out==NULL)
      map_write("map_out.4mb");
   else
      map_write(path_out);
   //-------------------------------------

   free(level_texture);
   free(level_floor);
   free(level_ceiling);

   HLH_json_free(json_data);
   HLH_json_free(json);

   return 0;
}

static void map_write(const char *path)
{
   //Calculate needed memory
   int size = 0;
   size+=4; //player_x
   size+=4; //player_y
   size+=4; //player_z
   size+=4; //player_angle
   size+=2; //level_width
   size+=2; //level_height
   size+=1; //level_floor_color
   size+=level_width*level_height*2; //level_texture
   size+=level_width*level_height; //level_floor
   size+=level_width*level_height; //level_ceiling

   uint8_t *mem = malloc(size);
   int pos = 0;

   //Write player x,y,z and angle
   *(int32_t *)(mem+pos) = player_x; pos+=4;
   *(int32_t *)(mem+pos) = player_y; pos+=4;
   *(int32_t *)(mem+pos) = player_z; pos+=4;
   *(int32_t *)(mem+pos) = player_angle; pos+=4;

   //Write width and height
   *(uint16_t *)(mem+pos) = level_width; pos+=2;
   *(uint16_t *)(mem+pos) = level_height; pos+=2;

   //Write floor color
   *(uint8_t *)(mem+pos) = level_floor_color; pos+=1;

   //Write texture, floor and ceiling
   memcpy(mem+pos,level_texture,level_width*level_height*2); pos+=level_width*level_height*2;
   memcpy(mem+pos,level_floor,level_width*level_height); pos+=level_width*level_height;
   memcpy(mem+pos,level_ceiling,level_width*level_height); pos+=level_width*level_height;

   //Compress and write to disk
   FILE *f = fopen(path,"wb");
   util_mem_compress(mem,size,f);
   fclose(f);

   //Free temp buffer
   free(mem);
}

static void util_mem_compress(void *mem, int32_t length, FILE *out)
{
   char *buffer_out = malloc(length+1);

   fwrite(&length,4,1,out);
   int32_t size = crush_encode(mem,length,buffer_out,length,9);
   fwrite(buffer_out,size,1,out);

   free(buffer_out);
}

static void print_help(char **argv)
{
   printf("%s usage:\n"
          "%s -project filename -level filename ]\n"
          "   -project\tpath to project file\n"
          "   -level\tpath to level file\n"
          "   -fout\tpath to save map to\n",
         argv[0],argv[0]);
}
//-------------------------------------
