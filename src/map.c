/*
A fps in 4mb

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <SLK/SLK.h>
//-------------------------------------

//Internal includes
#include "RvR_config.h"
#include "RvR_fix22.h"
#include "RvR_malloc.h"
#include "RvR_compress.h"
#include "RvR_pak.h"
#include "RvR_texture.h"
#include "player.h"
#include "map.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
uint16_t *level_texture = NULL;
int8_t *level_floor = NULL;
int8_t *level_ceiling = NULL;
uint16_t level_width = 0;
uint16_t level_height = 0;
uint8_t level_floor_color = 23;

static struct
{
   uint16_t *level_texture;
   int8_t *level_floor;
   int8_t *level_ceiling;
   uint16_t level_width;
   uint16_t level_height;
   uint8_t level_floor_color;
   RvR_fix22 player_x;
   RvR_fix22 player_y;
   RvR_fix22 player_z;
   RvR_fix22 player_angle;
}editor_map = {0};
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

void map_create(uint16_t width, uint16_t height)
{
   map_free_editor();
   map_free_level();

   editor_map.level_width = width;
   editor_map.level_height = height;
   editor_map.level_texture = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_texture));
   editor_map.level_floor = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_floor));
   editor_map.level_ceiling = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_ceiling));
   memset(editor_map.level_texture,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_texture));
   memset(editor_map.level_floor,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_floor));
   memset(editor_map.level_ceiling,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_ceiling));

   editor_map.player_x = 0;
   editor_map.player_y = 0;
   editor_map.player_z = 0;
   editor_map.player_angle = 0;
   editor_map.level_floor_color = 23;

   for(int y = 1;y<editor_map.level_height-1;y++)
   {
      for(int x = 1;x<editor_map.level_width-1;x++)
      {
         editor_map.level_floor[y*editor_map.level_width+x] = 0;
         editor_map.level_ceiling[y*editor_map.level_width+x] = 127;
      }
   }

   map_reset_full();
}

void map_reset()
{
   if(level_width!=editor_map.level_width||level_height!=editor_map.level_height|| //Wrong dimensions
      level_floor==NULL||level_ceiling==NULL||level_texture==NULL //Not allocated yet
      ) 
   {
      map_free_level();

      level_width = editor_map.level_width;
      level_height = editor_map.level_height;
      level_texture = RvR_malloc(level_width*level_height*sizeof(*level_texture));
      level_floor = RvR_malloc(level_width*level_height*sizeof(*level_floor));
      level_ceiling = RvR_malloc(level_width*level_height*sizeof(*level_ceiling));
      memset(level_texture,0,level_width*level_height*sizeof(*level_texture));
      memset(level_floor,0,level_width*level_height*sizeof(*level_floor));
      memset(level_ceiling,0,level_width*level_height*sizeof(*level_ceiling));
   }

   //Misc
   level_floor_color = editor_map.level_floor_color;

   memcpy(level_texture,editor_map.level_texture,sizeof(*editor_map.level_texture)*editor_map.level_width*editor_map.level_height);
   memcpy(level_floor,editor_map.level_floor,sizeof(*editor_map.level_floor)*editor_map.level_width*editor_map.level_height);
   memcpy(level_ceiling,editor_map.level_ceiling,sizeof(*editor_map.level_ceiling)*editor_map.level_width*editor_map.level_height);

   //Textures
   RvR_texture_load_begin();
   RvR_texture_load(0); //Texture 0 is always loaded, since it's used as a fallback texture
   for(int i = 0;i<level_width*level_height;i++)
      RvR_texture_load(level_texture[i]);
   RvR_texture_load_end();

   //TODO: do we want to do this here?
   RvR_pak_flush();
}

void map_free_editor()
{
   if(editor_map.level_texture!=NULL)
      RvR_free(editor_map.level_texture);
   if(editor_map.level_floor!=NULL)
      RvR_free(editor_map.level_floor);
   if(editor_map.level_ceiling!=NULL)
      RvR_free(editor_map.level_ceiling);
   editor_map.level_texture = NULL;
   editor_map.level_floor = NULL;
   editor_map.level_ceiling = NULL;
   editor_map.level_width = 0;
   editor_map.level_height = 0;
}

void map_free_level()
{
   if(level_texture!=NULL)
      RvR_free(level_texture);
   if(level_floor!=NULL)
      RvR_free(level_floor);
   if(level_ceiling!=NULL)
      RvR_free(level_ceiling);
   level_texture = NULL;
   level_floor = NULL;
   level_ceiling = NULL;
   level_width = 0;
   level_height = 0;
}

void map_save(const char *path)
{
   //Calculate needed memory
   int size = 0;
   size+=4; //player_x
   size+=4; //player_y
   size+=4; //player_z
   size+=4; //player_angle
   size+=2; //level_width
   size+=2; //level_height
   size+=1; //level_floor_color
   size+=editor_map.level_width*editor_map.level_height*2; //level_texture
   size+=editor_map.level_width*editor_map.level_height; //level_floor
   size+=editor_map.level_width*editor_map.level_height; //level_ceiling

   uint8_t *mem = RvR_malloc(size);
   int pos = 0;

   //Write player x,y,z and angle
   *(RvR_fix22 *)(mem+pos) = editor_map.player_x; pos+=4;
   *(RvR_fix22 *)(mem+pos) = editor_map.player_y; pos+=4;
   *(RvR_fix22 *)(mem+pos) = editor_map.player_z; pos+=4;
   *(RvR_fix22 *)(mem+pos) = editor_map.player_angle; pos+=4;

   //Write width and height
   *(uint16_t *)(mem+pos) = editor_map.level_width; pos+=2;
   *(uint16_t *)(mem+pos) = editor_map.level_height; pos+=2;

   //Write floor color
   *(uint8_t *)(mem+pos) = editor_map.level_floor_color; pos+=1;

   //Write texture, floor and ceiling
   memcpy(mem+pos,editor_map.level_texture,editor_map.level_width*editor_map.level_height*2); pos+=editor_map.level_width*editor_map.level_height*2;
   memcpy(mem+pos,editor_map.level_floor,editor_map.level_width*editor_map.level_height); pos+=editor_map.level_width*editor_map.level_height;
   memcpy(mem+pos,editor_map.level_ceiling,editor_map.level_width*editor_map.level_height); pos+=editor_map.level_width*editor_map.level_height;

   //Compress and write to disk
   FILE *f = fopen(path,"wb");
   RvR_mem_compress(mem,size,f);
   fclose(f);

   //Free temp buffer
   RvR_free(mem);
}

void map_load(const char *path)
{
   int size = 0;
   int pos = 0;
   uint8_t *mem = RvR_decompress_path(path,&size);

   //Read player x,y,z and angle
   editor_map.player_x = *(RvR_fix22 *)(mem+pos); pos+=4;
   editor_map.player_y = *(RvR_fix22 *)(mem+pos); pos+=4;
   editor_map.player_z = *(RvR_fix22 *)(mem+pos); pos+=4;
   editor_map.player_angle = *(RvR_fix22 *)(mem+pos); pos+=4;
   
   //Read level width and height
   editor_map.level_width = *(uint16_t *)(mem+pos); pos+=2;
   editor_map.level_height = *(uint16_t *)(mem+pos); pos+=2;

   //Read floor color
   editor_map.level_floor_color = *(uint8_t *)(mem+pos); pos+=1;

   //Read texture, floor and ceiling
   editor_map.level_texture = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_texture));
   editor_map.level_floor = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_floor));
   editor_map.level_ceiling = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_ceiling));
   memcpy(editor_map.level_texture,mem+pos,editor_map.level_width*editor_map.level_height*2); pos+=editor_map.level_width*editor_map.level_height*2;
   memcpy(editor_map.level_floor,mem+pos,editor_map.level_width*editor_map.level_height); pos+=editor_map.level_width*editor_map.level_height;
   memcpy(editor_map.level_ceiling,mem+pos,editor_map.level_width*editor_map.level_height); pos+=editor_map.level_width*editor_map.level_height;

   RvR_free(mem);

   //Update actual map
   map_reset_full();
}

void map_reset_full()
{
   map_reset();

   player.x = editor_map.player_x;
   player.y = editor_map.player_y;
   player.z = editor_map.player_z;
   player.direction = editor_map.player_angle;
   player.shear = 0;
}
//-------------------------------------
