/*
A fps in 4mb

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <SLK/SLK.h>
//-------------------------------------

//Internal includes
#include "RvR_config.h"
#include "RvR_fix22.h"
#include "mode.h"
#include "game_draw.h"
#include "game.h"
#include "map.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
typedef enum
{
   MODE_TITLE, MODE_CREDITS, MODE_EPSEL, MODE_DIFFSEL, MODE_GAME,
}Game_state;
//-------------------------------------

//Variables
static Game_state mode = 0;
//-------------------------------------

//Function prototypes
static void mode_title();
static void mode_credits();
static void mode_epsel();
static void mode_diffsel();
static void mode_game();
//-------------------------------------

//Function implementations

void mode_update()
{
   switch(mode)
   {
   case MODE_TITLE: mode_title(); break;
   case MODE_CREDITS: mode_credits(); break;
   case MODE_EPSEL: mode_epsel(); break;
   case MODE_DIFFSEL: mode_diffsel(); break;
   case MODE_GAME: mode_game(); break;
   }
}

static void mode_title()
{
   static int selected = 0;

   //Graphics
   SLK_layer_set_current(0);
   SLK_draw_pal_clear();
   SLK_draw_pal_string((XRES-200)/2,YRES/6,5,"Title",78);
   SLK_draw_pal_string((XRES-80)/2,YRES/2,2,"Start",78);
   SLK_draw_pal_string((XRES-112)/2,YRES/2+YRES/12,2,"Credits",78);
   SLK_draw_pal_string((XRES-64)/2,YRES/2+YRES/6,2,"Quit",78);
   switch(selected)
   {
   case 0: SLK_draw_pal_string((XRES-112)/2,YRES/2,2,">     <",78); break;
   case 1: SLK_draw_pal_string((XRES-144)/2,YRES/2+YRES/12,2,">       <",78); break;
   case 2: SLK_draw_pal_string((XRES-96)/2,YRES/2+YRES/6,2,">    <",78); break;
   }

   //Input
   if(SLK_key_pressed(SLK_KEY_DOWN)&&selected<2)
      selected++;
   if(SLK_key_pressed(SLK_KEY_UP)&&selected>0)
      selected--;
   if(SLK_key_pressed(SLK_KEY_ENTER))
   {
      switch(selected)
      {
      case 0: mode = MODE_EPSEL; break;
      case 1: mode = MODE_CREDITS; break;
      case 2: SLK_core_quit(); break;
      }
   }
}

static void mode_credits()
{
   //Graphics
   SLK_layer_set_current(0);
   SLK_draw_pal_clear();
   SLK_draw_pal_string((XRES-280)/2,YRES/8,5,"Credits",78);
   SLK_draw_pal_string((XRES-560)/2,YRES/3,1,"H. Lukas Holzbeierlein (Captain4LK)",78);

   //Input
   if(SLK_key_pressed(SLK_KEY_ENTER)||SLK_key_pressed(SLK_KEY_ESCAPE))
      mode = MODE_TITLE;
}

static void mode_epsel()
{
   static int selected = 0;

   //Graphics
   SLK_layer_set_current(0);
   SLK_draw_pal_clear();

   //Input
   if(SLK_key_pressed(SLK_KEY_ENTER))
   {
      set_episode(selected);
      mode = MODE_DIFFSEL;
   }
}

static void mode_diffsel()
{
   static int selected = 0;

   //Graphics
   SLK_layer_set_current(0);
   SLK_draw_pal_clear();

   //Input
   if(SLK_key_pressed(SLK_KEY_ENTER))
   {
      set_difficulty(selected);
      mode = MODE_GAME;      
   }
}

static void mode_game()
{
   //Graphics
   SLK_layer_set_current(0);
   game_draw();

   //Game logic
   game_update();
}
//-------------------------------------
