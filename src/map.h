/*
A fps in 4mb

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

#ifndef _MAP_H_

#define _MAP_H_

extern uint16_t *level_texture;
extern int8_t *level_floor;
extern int8_t *level_ceiling;
extern uint16_t level_width;
extern uint16_t level_height;
extern uint8_t level_floor_color;

void map_create(uint16_t width, uint16_t height);
void map_reset();
void map_reset_full();
void map_free_editor();
void map_free_level();
void map_save(const char *path);
void map_load(const char *path);

#endif
