/*
RvnicRaven retro game engine

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <string.h>
#include <SLK/SLK.h>
//-------------------------------------

//Internal includes
#include "RvR_config.h"
#include "RvR_error.h"
#include "RvR_fix22.h"
#include "RvR_malloc.h"
#include "RvR_map.h"
//-------------------------------------

//#defines
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
static uint16_t *level_texture = NULL;
static int8_t *level_floor = NULL;
static int8_t *level_ceiling = NULL;
static uint16_t level_width = 0;
static uint16_t level_height = 0;
static uint8_t level_floor_color = 23;

static struct
{
   uint16_t *level_texture;
   int8_t *level_floor;
   int8_t *level_ceiling;
   uint16_t level_width;
   uint16_t level_height;
   uint8_t level_floor_color;
   RvR_fix22 player_x;
   RvR_fix22 player_y;
   RvR_fix22 player_z;
   RvR_fix22 player_angle;
}editor_map = {0};
//-------------------------------------

//Function prototypes
static void map_free_editor();
static void map_free_level();
//-------------------------------------

//Function implementations

void RvR_map_create(uint16_t width, uint16_t height)
{
   map_free_editor();
   map_free_level();

   editor_map.level_width = width;
   editor_map.level_height = height;
   editor_map.level_texture = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_texture));
   editor_map.level_floor = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_floor));
   editor_map.level_ceiling = RvR_malloc(editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_ceiling));
   memset(editor_map.level_texture,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_texture));
   memset(editor_map.level_floor,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_floor));
   memset(editor_map.level_ceiling,0,editor_map.level_width*editor_map.level_height*sizeof(*editor_map.level_ceiling));

   editor_map.player_x = 0;
   editor_map.player_y = 0;
   editor_map.player_z = 0;
   editor_map.player_angle = 0;
   editor_map.level_floor_color = 23;

   for(int y = 1;y<editor_map.level_height-1;y++)
   {
      for(int x = 1;x<editor_map.level_width-1;x++)
      {
         editor_map.level_floor[y*editor_map.level_width+x] = 0;
         editor_map.level_ceiling[y*editor_map.level_width+x] = 127;
      }
   }

   //TODO: don't call this here, make user call this
   //If I don't reset the map myself, it is possible to cache a map by loading it, but not reseting
   RvR_map_reset_full();
}

static void map_free_editor()
{
   if(editor_map.level_texture!=NULL)
      RvR_free(editor_map.level_texture);
   if(editor_map.level_floor!=NULL)
      RvR_free(editor_map.level_floor);
   if(editor_map.level_ceiling!=NULL)
      RvR_free(editor_map.level_ceiling);
   editor_map.level_texture = NULL;
   editor_map.level_floor = NULL;
   editor_map.level_ceiling = NULL;
   editor_map.level_width = 0;
   editor_map.level_height = 0;
}

static void map_free_level()
{
   if(level_texture!=NULL)
      RvR_free(level_texture);
   if(level_floor!=NULL)
      RvR_free(level_floor);
   if(level_ceiling!=NULL)
      RvR_free(level_ceiling);
   level_texture = NULL;
   level_floor = NULL;
   level_ceiling = NULL;
   level_width = 0;
   level_height = 0;
}

void RvR_map_reset()
{

}

void RvR_map_reset_full()
{

}
//-------------------------------------
