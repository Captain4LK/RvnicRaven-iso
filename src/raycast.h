/*
A fps in 4mb

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

#ifndef _RAYCAST_H_

#define _RAYCAST_H_

#define RCL_COMPUTE_WALL_TEXCOORDS 1
#define RCL_COMPUTE_FLOOR_TEXCOORDS 0
#define RCL_TEXTURE_VERTICAL_STRETCH 0
#define RCL_COMPUTE_FLOOR_DEPTH 1
#define RCL_COMPUTE_CEILING_DEPTH 1
#define RCL_ROLL_TEXTURE_COORDS 1
#define RCL_HORIZONTAL_FOV 256
#define RCL_VERTICAL_FOV 330
#define RCL_CAMERA_COLL_HEIGHT_BELOW 800
#define RCL_CAMERA_COLL_HEIGHT_ABOVE 200
#define RCL_CAMERA_COLL_STEP_HEIGHT (RCL_UNITS_PER_SQUARE/4)
#include "../external/raycastlib.h"

#endif
