/*
RvnicRaven retro game engine

Written in 2021 by Lukas Holzbeierlein (Captain4LK) email: captain4lk [at] tutanota [dot] com

To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>. 
*/

//External includes
#include <stdio.h>
#include <string.h>
#include <SLK/SLK.h>
//-------------------------------------

//Internal includes
#include "RvR_config.h"
#include "RvR_error.h"
#include "RvR_fix22.h"
//-------------------------------------

//#defines
#define FIX22_ONE (1<<10)
//-------------------------------------

//Typedefs
//-------------------------------------

//Variables
//-------------------------------------

//Function prototypes
//-------------------------------------

//Function implementations

RvR_fix22 RvR_fix22_from_int(int a)
{
   return (RvR_fix22)a*FIX22_ONE;
}

int RvR_fix22_to_int(RvR_fix22 a)
{
   return (int)(a/FIX22_ONE);
}

RvR_fix22 RvR_fix22_mul(RvR_fix22 a, RvR_fix22 b)
{
   int64_t p = (int64_t)a*(int64_t)b;

   return (RvR_fix22)(p/(int64_t)FIX22_ONE);
}

RvR_fix22 RvR_fix22_div(RvR_fix22 a, RvR_fix22 b)
{
   int64_t p = (int64_t)a*(int64_t)FIX22_ONE;

   return (RvR_fix22)(p/(int64_t)b);
}
//-------------------------------------
